import { useState } from 'react'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import './App.css'
import Home from './Home';
import Options from './Options';

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/pregnant",
    element: <Options />,
  },
  {
    path: "/baby",
    element: <Options />,
  },
  {
    path: "/kid",
    element: <Options />,
  },
]);

function App() {

  return (
    <RouterProvider router={router} />
  )
}

export default App
