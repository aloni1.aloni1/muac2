import { Button, Box, Fab, Stack } from '@mui/material';
import { useNavigate } from 'react-router';
import CameraAltOutlinedIcon from '@mui/icons-material/CameraAltOutlined';
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';

const Options = () => {
  const buttonsSize = {height: '20vh', width: '20vh'};
  const navigate = useNavigate();

	return (
		<>
			<Stack spacing={20}>
				<Fab onClick={() => navigate('Scan')} sx={{backgroundColor: '#f59fac', ...buttonsSize}}>
					<CameraAltOutlinedIcon sx={{fontSize: '8em'}} />
				</Fab>
				<Fab onClick={() => navigate('Fit')} sx={{backgroundColor: '#82cccf', ...buttonsSize}}>
					<Box
							component='img' 
							sx={{height: '50%'}} 
							src='../../assets/icon-measuring_tape.png' />
				</Fab>
			</Stack>
			<Button onClick={() => navigate('/')} sx={{position: 'absolute', bottom: 0, left: 0}}>
				<ArrowBackOutlinedIcon sx={{fontSize: '2em'}} />
			</Button>
		</>
	);
};

export default Options;